import apache_log_parser
import seaborn
import Levenshtein
from pandas import DataFrame
from matplotlib import pyplot

parser = apache_log_parser.make_parser('%h %l %u %t "%r" %>s')

data = {'ip': [],
        'query': [],
        'query_len': [],
        'reformulation': []}

with open('basic_web.log') as in_f:
    for line in in_f:
        line = parser(line)
        queries = line['request_url_query_dict']
        if 'q' in queries:
            query = queries['q'][0].lower()
            if len(data['ip']) > 0 and data['ip'][-1] == line['remote_host']:
                if data['query'][-1] in query:
                    data['reformulation'].append('specialisation')
                elif query in data['query'][-1]:
                    data['reformulation'].append('generalisation')
                    # elif Levenshtein.distance(data['query'][-1], query) <= 2:
                    #     data['reformulation'].append('spelling')
                else:
                    print(data['query'][-1], ' - ', query)
                        # q1 = set(data['query'][-1].split())
                        # q2 = set(query.split())
                        # shared = q1.intersection(q2)
                        # if len(shared) > 0:
                    data['reformulation'].append('new')
                        # data['reformulation'].append('parallel')
#                 else:
# #                         print(data['query'][-1], '-', query, Levenshtein.distance(data['query'][-1], query))
#                 data['reformulation'].append('new')
            else:
                data['reformulation'].append('new')
            data['ip'].append(line['remote_host'])
            data['query'].append(query)
            data['query_len'].append(len(query.split()))

data = DataFrame(data)

plot = seaborn.countplot(data=data, x='reformulation')
plot.get_figure().savefig('query_reformulation.png')
pyplot.close()
