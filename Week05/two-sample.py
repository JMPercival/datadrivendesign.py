import seaborn

from matplotlib import pyplot
from pandas import read_csv
from scipy import stats

data = read_csv('gardens.csv')

print(data)

plot = seaborn.boxplot(data=data[['gardenA', 'gardenB']])
plot.get_figure().savefig('garden_boxplot.png')
pyplot.close()

# Using the boxplot, looks as though the ozone distributions ae significally different

plot = seaborn.distplot(data['gardenA'])
plot.get_figure().savefig('garden_distplot_A.png')
pyplot.close()

plot = seaborn.distplot(data['gardenB'])
plot.get_figure().savefig('garden_distplot_B.png')
pyplot.close()

# Both distplot graphs are exactly the same, this means that the data is normally distributed

print(stats.levene(data['gardenA'], data['gardenB']))
# 'stats' is imported from scipt library, which provides a vast array of scientific analysis,
# the 'stats' module gives acces to the statistics-speific functionalityself.
# 'Levene' is a test that tests equal variances. (Read the Tutorial that covers this)

print(stats.ttest_ind(data['gardenA'], data['gardenB']))

print(stats.ttest_ind(data['gardenA'], data['gardenB'], equal_var = False))

print(stats.wilcoxon(data['gardenA'], data['gardenB']))
