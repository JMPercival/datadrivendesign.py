import numpy

from collections import Counter
from csv import DictReader
from sklearn import cluster

data = []

with open('cleaned.csv') as in_f:
    reader = DictReader(in_f)
    for line in reader:
        data.append([line['time_received_isoformat'].count('-'),
                     line['time_received_isoformat'].count(':'),
                     line['time_received_isoformat'].count(',')])


data = numpy.array(data)

clustering = cluster.AgglomerativeClustering(n_clusters=2000)
clustering.fit(data)

counts = [[] for _ in range(0, len(set(clustering.labels_)))]

with open('cleaned.csv') as in_f:
 reader = DictReader(in_f)
 for idx, line in enumerate(reader):
     counts[clustering.labels_[idx]].append(line['time_received_isoformat'])

for idx, count in enumerate(counts):
    print(idx, Counter(count))
