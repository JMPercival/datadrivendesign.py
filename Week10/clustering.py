import numpy

from sklearn import cluster

data = numpy.array([[3, 100],
                    [14, 200],
                    [16, 200],
                    [34, 200],
                    [11, 100],
                    [4, 100],
                    [30, 100],
                    [32, 100],
                    [6 , 100],
                    [40, 200]])

clustering = cluster.AgglomerativeClustering(n_clusters=6)
clustering.fit(data)

for (attrs, label) in zip(data, clustering.labels_):
    print(label, attrs)
