import apache_log_parser
import seaborn
from pandas import DataFrame
from matplotlib import pyplot

parser = apache_log_parser.make_parser('%h %l %u %t "%r" %>s')

data = {'category': [],
        'day': [],
        'time': []}

with open('basic_web.log') as in_f:
    for line in in_f:
        line = parser(line)

        data['day'].append(line['time_received_tz_datetimeobj'].day)
        data['time'].append(line['time_received_tz_datetimeobj'].hour * 60 + line['time_received_tz_datetimeobj'].minute)

        if 'search' in line['request_url_path']:
            data['category'].append('search')
        elif 'browse' in line['request_url_path']:
            data['category'].append('browse')
        elif 'add to-cart' in line['request_url_path'] or 'remove-from-cart' in line['request_url_path']:
            data['category'].append('cart')
        elif 'book' in line['request_url_path']:
            data['category'].append('item')
        elif line['request_url_path'] == '/baseline/':
            data['category'].append('baseline')
        elif line['request_url_path'] == '/explore/' or line['request_url_path'] == '/review/':
            data['category'].append('modern')
        else:
            data['category'].append('other')


data=DataFrame(data)

plot = seaborn.violinplot(data=data, x='category', y='day')
plot.get_figure().savefig('category_day_violinplot.png')
pyplot.close()

plot = seaborn.kdeplot(data=data.time[data.category=='search'] / 60.0, label='Search')
plot = seaborn.kdeplot(data=data.time[data.category=='browse'] / 60.0, label='Browse')
plot.get_figure().savefig('time_density.png')
pyplot.close()
