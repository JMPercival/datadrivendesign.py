from copy import deepcopy
from csv import DictReader,  DictWriter
from datetime import datetime
from collections import Counter
from numpy import array

with open('cleaned_log.csv') as in_f:       # opens the cleaned.log csv file
    with open('sessions.csv', 'w') as out_f:     # sessions file created that contains the session data
        reader = DictReader(in_f)       # Dict reader created this reads each line of the CSV as a dictionary

        fieldnames = deepcopy(reader.fieldnames)     #reads the fieldnames from the reader(by accessing reader.fieldnams)
        fieldnames.append('session')        #fieldnames are read and appended to 'session' field

        writer = DictWriter(out_f, fieldnames=fieldnames)  # used to define the feildnames used by the Dictwriter
        writer.writeheader()

## The sessions variable will hold the last access stamp and session identifier for each "remote_host"

        sessions = {}       #This new variable holds the last access timestamp and the session identifier for each "remote_host"
        for line in reader:     #in the for loop the timestamp is loaded for each request
            if 'Feedfetcher' in line['request_header_user_agent'] or 'AddThis' in line['request_header_user_agent'] or 'Springshare' in line['request_header_user_agent']:
                continue
            if '(end - start).seconds > 30000':
                continue
            timestamp = datetime.strptime(line['time_received_isoformat'], '%Y-%m-%dT%H:%M:%S')
        #The line above ^^ strings to parse the (time_received_isoformat) and the pattern the string uses. %Y = Year, month, day -
        #the "T" seperates the date and the time, which is in Hour, Minutes and Seconds

            if line['remote_host'] in sessions:
                delta = timestamp - sessions[line['remote_host']][0]
                #times differenc is calculated by finding the diference between the current timestamp and the previous
                #this is available from the delta line above ^^

                if delta.days > 0 or delta.seconds > 1800:
                #.days specidies no of days bewtween thw 2 timestamps, .seconds specifies the number of seconds between the 2.
                #using the delta line above ^^ if there were more than 1 day between request(defo more than half an hr)
                #delta.seconds > 1800 . .. (1800/60 == 30 minutes)

                    sessions[line['remote_host']] = (timestamp, sessions[line['remote_host']][1] + 1)
                #if either are true, we update the tuple ( sessions[line['remote_host']] ) to the current time stamp and the current timestamp + 1
                #(sessions[line['remote_host']][1] + 1])) - this means that everytime the time difference between each request is greater than 30 minutes
                # the session identifier is incremented, identifying the request as part of a new session.

                else:
                    sessions[line['remote_host']] = (timestamp, sessions[line['remote_host']][1])
                #This deals with cases were the timestamp difference is less than 1/2 an hr, normally we wouldnt have to do anything else here.
                #However we need to put someting to elliminate any future requests.
            else:
                sessions[line['remote_host']] = (timestamp, 1)
            line['session'] = sessions[line['remote_host']][1]
            writer.writerow(line)
####Run this Libre office calculator!!!vIt shows how many requests belong to the users first session, there are somewer incidents were the user has returned a second time
