from copy import deepcopy
from csv import DictReader, DictWriter
from datetime import datetime
from collections import Counter
from numpy import array

with open ('sessions.csv') as in_f: #sessions.csv is opened and fed into DictReader
    reader = DictReader(in_f)
    hits = {}       # Directory called "hits is created" to count how many "hits" (requests) each session has
    session_lengths = {}
    for line in reader:     # We then loop through the lines in the reader
        identifier = '%s_%s' % (line['remote_host'], line['session']) # new variable(identifier) created
        #^^ This is used to to act as a unique identifier for a specific session. String substitution is used to combine,
        # the "remote_host" and the "session" fields to create the unique identifier

        timestamp = datetime.strptime(line['time_received_isoformat'], '%Y-%m-%dT%H:%M:%S')

        if identifier in hits:  #we check if the session has been seen, by checking the identifier already exists in the hits dictionary
            hits[identifier] = hits[identifier] + 1
        else:                   #if it does not we initialise the "hits" for the identifier to 1. If it does already exist the we icrement it by one
            hits[identifier] = 1
        if identifier in session_lengths:       #here we use session_lengths dictionary to store the data
            session_lengths[identifier] = (session_lengths[identifier][0], timestamp)
        #^^ When we see that an identifier already exists in session_lengths. we update the tuple(start time) and current timestamp as the end time
        # ensuring that at the end of the loop the session_lengths dict contains start and end timestamps for each session.
        else:
            session_lengths[identifier] = (timestamp, timestamp)

            # at the end of the loop "hits" variable will contain ALL the number of hits (requests) in each session.


    print(Counter(hits.values()))
    session_lengths = [(end-start).seconds for (start, end) in session_lengths.values()]
    session_lengths = array(session_lengths)
    print(session_lengths.max() /60) # results= 1439 minutes, nrly 24hrs- concludes at least 1 session that runs all day(strong indication that we are dealing with a robot)
    print(session_lengths.mean() /60)
    print(session_lengths.std() /60)
    print(Counter(hits.values()))
    for key, (start, end) in session_lengths.items():
        if (end - start).seconds > 30000:
            print(key)

            #In the results - the "keys" are the session lengths, the values are the session requests counts.
