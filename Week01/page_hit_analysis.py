import apache_log_parser
from collections import Counter
from pandas import DataFrame
import seaborn

parser = apache_log_parser.make_parser('%h %l %u %t %r %>s')

pages = []
with open('basic_web.log') as in_f:
    for line in in_f:
        line = parser(line)
        pages.append(line["request_url_path"])

counts = Counter(pages)

selected_pages = [page for page, _ in counts.most_common(5)]
print(selected_pages)

graph_pages = [page for page in pages if page in selected_pages]
data = DataFrame({'pages': graph_pages})
print(data)

plot = seaborn.countplot(data=data, y='pages', order=selected_pages)
plot.get_figure().savefig('pages_plot.png')
