import apache_log_parser

from csv import DictWriter

parser = apache_log_parser.make_parser('%h %l %u %t "%r" %>s "%{User-agent}i"')

with open('cleaned_log.csv', 'w') as out_f:
    writer = DictWriter(out_f,
                        fieldnames=['remote_host',
                                    'time_received_isoformat',
                                    'request_method',
                                    'request_url_path',
                                    'request_url_query',
                                    'status',
                                    'request_header_user_agent'],
                        extrasaction='ignore')
    writer.writeheader()

    ip_map = {}
    with open('nlm_sample.log') as in_f:
      for line in in_f:
        line = parser(line)
        ip_addr = line['remote_host']
#
# The following 3 lines introduce the 'ip_map' variable, to which the empty dictionary is assigned to.
# The 'ip_map' stores the mapping ip address that is in the log data, to be anononymised unique identifier
# that will be used in the data produced.
#
        if ip_addr not in ip_map:
            ip_map[ip_addr] = len(ip_map) + 1
    line['remote_host'] = ip_map[ip_addr]
    writer.writerow(line)
