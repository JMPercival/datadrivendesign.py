import apache_log_parser
import user-agent
import re

from csv import DictWriter

parser = apache_log_parser.make_parser('%h %l %u %t "%r" %>s "%{User-agent}i"')

CREDIT_CARD = re.compile(r'[0-9]{4}-[0-9]{4}-[0-9]{4}-[0-9]{4}')
EMAIL = re.compile(r"(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-

with open('cleaned.csv', 'w') as out_f:
    writer = DictWriter(out_f,
                        fieldnames=['remote_host',
                                    'time_received_isoformat',
                                    'request_method',
                                    'request_url_path',
                                    'request_url_query',
                                    'status',
                                    'request_header_user_agent'],
                        extrasaction='ignore')
    writer.writeheader()

    ip_map = {}
    with open('nlm_sample.log') as in_f:
      for line in in_f:
        line = parser(line)
        ua = user_agents.parse(line['request_header_user_agent'])
        if not ua.is_bot:
            ip_addr = line['remote_host']
            if ip_addr not in ip_map:
                ip_map[ip_addr] = len(ip_map) + 1
            line['remote_host'] = ip_map[ip_addr]
            if 'request_url_query' in line:
                if CREDIT_CARD.search(line['request_url_query'])
                    print('Anonymising credir card number')
                    line['request_url_query'] = CREDIT_CARD.sub(line['request_url_query'], 'XXXX-XXXX-XXXX-XXXX')
                if EMAIL.search(line['request_url_query'])
                    print('Anoonymising e-mail address')
                    line['request_url_query'] = EMAIL.sub(line['request_url_query'], 'user@example.com')
            writer.writerow(line)

    print(len(ip_map))
