import re

# pattern = re.compile(r'a')
#
# print(pattern.search('Hall'))
# print(pattern.search('Hell'))
#
# pattern = re.compile(r'([0-9]+\s?)+1')
# print(pattern.search('0342 212 321').group(0))

pattern = re.compile(r'([a-zA-Z]+).*(ce[0-9]+)')
match = pattern.search('Hall - ce219')
print(match.group(0))
print(match.group(1))
print(match.group(2))
