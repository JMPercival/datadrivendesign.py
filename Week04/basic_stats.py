import math
import numpy
import seaborn
from matplotlib import pyplot

sample_a = [numpy.random.randint(1, 10) for _ in range(0, 20)]
sample_b = [numpy.random.randint(4, 8) for _ in range(0, 21)]
# numpy.random.randint() = functin is called 20(21) time, giving me a list of 21-21
# random numbers.
print(sample_a)
print(sample_b)

plot = seaborn.distplot(sample_a, bins=10)
plot.get_figure().savefig('distplot_a.png')
pyplot.close()
plot = seaborn.distplot(sample_b)
plot.get_figure().savefig('distplot_b.png')
pyplot.close()

def min_max(sample):
    min_value = None
    max_value = None

    for value in sample:
        if min_value is None or value < min_value:
            min_value = value
        if max_value is None or value > max_value:
            max_value = value

        return min_value, max_value

print(min_max(sample_a))
print(min_max(sample_b))
# The code below will do it too, the aim here is t learn how this below works,
#and how to use it using the code below
# print(numpy.min(sample_a), numpy.max(sample_a))
# print(numpy.min(sample_b), numpy.max(sample_b))

def mean(sample):
    total = 0
    for value in sample:
        total = total + value
    return total / len(sample)

def stddev(sample) :
    mn = mean(sample)
    var_samples = [math.pow(value - mn, 2) for value in sample]
    var = mean(var_samples)
    return math.sqrt(var)

print(mean(sample_a), stddev(sample_a))
print(mean(sample_b), stddev(sample_b))

# again numpy provide functions and printlines, this is to show how it works
# print(numpy.mean(sample_a), numpy.stddev(sample_a))
# print(numpy.mean(sample_b), numpy.stddev(sample_b))

def percentile(sample, perc):
    sample.sort()
    idx = (len(sample) - 1) * perc / 100.0
    if int(idx) == idx:
        return sample[int(idx)]
    else:
        return (sample[math.floor(idx)] + sample[math.ceil(idx)]) / 2.0
# 
# print(percentile(sample_a, 25),numpy.percentile(sample_a, 50),numpy.percentile(sample_a, 75))
# print(percentile(sample_b, 25),numpy.percentile(sample_b, 50),numpy.percentile(sample_b, 75))

print(numpy.percentile(sample_a, 25),numpy.percentile(sample_a, 50),numpy.percentile(sample_a, 75))
print(numpy.percentile(sample_b, 25),numpy.percentile(sample_b, 50),numpy.percentile(sample_b, 75))
